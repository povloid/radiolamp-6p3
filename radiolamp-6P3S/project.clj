(defproject radiolamp-6P3S "0.4.0"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url  "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojurescript "1.9.494"]
                 [com.cognitect/transit-cljs "0.8.239"]
                 [org.omcljs/om "0.9.0"]
                 [secretary "1.2.3"]
                 [cljsjs/d3 "3.5.16-0"]
                 [markdown-clj "0.9.95"]])
